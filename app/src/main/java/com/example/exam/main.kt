package com.example.exam

fun main(args: Array<String>) {
    var x = 1235
    var sum = 0

    while (x > 0) {
        var y = x % 10
        sum += y
        x /= 10
    }

    println(sum)

}